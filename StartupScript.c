#include "filegpio/filegpio.h"

#include <stdio.h>
#include <stdlib.h>
// voeg gpio toe in int vorm. als laatste -1 voor init loop
static int gpio_out[] ={	66, 67, 69, 68, 45, 44, 23, 26,
							47, 46, 27, 30, 60, 31, 50, 19,
							-1 };
static int gpio_in[] = {	65, 48, -1};

int main(){

	int i = 0;
	//while not end of array, export and set direction
	while ( gpio_out[i] >= 0 ){
		export_gpio(gpio_out[i]);
		set_direction(gpio_out[i], 1);
		i++;
	}

	//same for input.
	i = 0;
	while (  gpio_in[i] >= 0 ){
		export_gpio(gpio_in[i]);
		set_direction(gpio_in[i], 0);
		i++;
	}

	// set permissions for gpio export and individual gpio
	system("chgrp -R scanners /sys/class/gpio/");
	system("chgrp -R scanners /sys/devices/virtual/gpio");
	return 0;
}
