//==============================================================================
//  Author      : Peter Buijs
//  Description : The main functions for 3D scanning.
//                Call Scan_3D() to make a scan
//==============================================================================

#include "Scan_data.h"
#include "FrameGrabberThread.h"
#include "Debug.h"
#include "ImageProcessing.h"
#include "Hoofdprogramma.h"
#include "../MOTORdriver/MOTORdriver.h"
#include "opencv2/core/core_c.h"

#include <pthread.h>

// globals
// pointer for communication with frame grabber thread
// This pointer is not always valid, even when not NULL!
extern pthread_t CameraThread; // Camera thread handle
IplImage *pReferenceImage;  // used for dirrerence calculation

int Init_Scan_data( int Height, int Width, struct Scan_data **ppCurrent_Scan )
{
    *ppCurrent_Scan = malloc( sizeof(struct Scan_data) );   // allocate struct
    if( *ppCurrent_Scan == NULL )
        return ERR_MALLOC;
    Malloc_debug( "*ppCurrent_Scan", *ppCurrent_Scan, sizeof(struct Scan_data) );

    (*ppCurrent_Scan)->Data = malloc( Width * sizeof( float * ) );  // allocate array of float pointers
    if( (*ppCurrent_Scan)->Data == NULL )
        return ERR_MALLOC;
    Malloc_debug( "*ppCurrent_Scan", *ppCurrent_Scan, sizeof(struct Scan_data) );

    (*ppCurrent_Scan)->Height = Height;
    (*ppCurrent_Scan)->Width  = Width;
    (*ppCurrent_Scan)->Data   = malloc( Height*Width*sizeof(float) );
    if( (*ppCurrent_Scan)->Data == NULL )
        return ERR_MALLOC;
    Malloc_debug( "(*ppCurrent_Scan)->Data", (*ppCurrent_Scan)->Data, Height*Width*sizeof(float) );

    return 0;
}

int Scan_3D( struct Scan_data **ppCurrent_Scan )
{
    int Err=0, i;
    struct Col_data *pCurrent_Col;
    float **Current_Scan_data;

    if( Err |= Init_Scan_data( IMAGE_HEIGHT, IMAGE_WIDTH, ppCurrent_Scan ) )
        return Err;
    Current_Scan_data = (*ppCurrent_Scan)->Data;

    for( i=0; i<IMAGE_WIDTH; i++)
    {
        printf("Scanning... %3d/%3d\r\n", i+1, IMAGE_WIDTH);
        Debug_CPUtime( "Scan_3D loop running" );

        Err |= Scan_Column( &pCurrent_Col );

        Current_Scan_data[i] = pCurrent_Col->Data;    // add scanned column data to the full scan sctruct

        free( pCurrent_Col );
        Free_debug( "pCurrent_Col", pCurrent_Col );

        Err |= Set_Progress( 5 * (int)( i / ((float)IMAGE_WIDTH/(100/5)) ) ); // set progress percentage per 5%

        if( Err )
            break;
    }

    motor_return(); // unentangle the laser wires again after rotating

    return Err;
}
