//==============================================================================
//  Author      : Peter Buijs
//  Description : External functions for image processing
//==============================================================================
#include "opencv2/core/core_c.h"

int Scan_Column( struct Col_data ** );
int GetImage_from_CameraThread( IplImage ** );

void Just_take_a_damn_picture( void );
