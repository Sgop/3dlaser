//==============================================================================
//  Author      : Peter Buijs
//  Description : Functions used to track malloc and free calls,
//                used for debugging memory leaks.
//==============================================================================
#include <stdio.h>
#include <time.h>

#include "Debug.h"
#include "opencv2/core/types_c.h"

#ifdef DEBUG
    char *Get_ErrorText( int Err )
    {
        switch( Err )
        {
            case 0      : return "Information";
            case 1<< 0  : return "Not yet implemented";
            case 1<< 1  : return "Default case used in switch";
            case 1<< 2  : return "LCD text not exactly 16 characters";
            case 1<< 3  : return "Buffer too small";
            case 1<< 4  : return "Cannot get FS stats";
            case 1<< 5  : return "Worker thread died";
            case 1<< 6  : return "Could not open camera";
            case 1<< 7  : return "Could not grab frame";
            case 1<< 8  : return "Image dimentions do not match";
            case 1<< 9  : return "Malloc was unsuccesful";
            case 1<<10  : return "User requested termination";
            case 1<<11  : return "Error scanning";
            case 1<<12  : return "Cannot save .PLY file";
            default     : return "Multiple errors";
        }
    }
#else

#endif //DEBUG

#ifdef DEBUG_MEMLEAK
    void Malloc_debug( char *Message, void *Ptr, int Size )
    {
        fprintf( stderr, "[MEMORY] Allocated %d bytes for %s at address %p\r\n", Size, Message, Ptr );
    }
    void Free_debug( char *Message, void *Ptr )
    {
        fprintf( stderr, "[MEMORY] Freed %s from address %p\r\n", Message, Ptr );
    }
#else
    void Malloc_debug( char *Message, void *Ptr, int Size ){}
    void Free_debug( char *Message, void *Ptr ){}
#endif // DEBUG_MEMLEAK

#ifdef DEBUG_IMAGES
    void DebugSaveImg( const char *Path, IplImage *pImage )
    {
        Debugprint( Path, 0 );
        const int params = 0;
        cvSaveImage( Path, pImage, &params );
    }
#else
    DebugSaveImg( const char *Path, IplImage *pImage ){}
#endif //DEBUG_IMAGES

#ifdef DEBUG_PERFORMANCE
    void Debug_CPUtime( char *message )
    {
        static clock_t oldtime_ms, newtime_ms = 0;
        oldtime_ms = newtime_ms;
        newtime_ms = clock()/1000;
        fprintf( stderr, "[Timestamp] %ju ms (+%ju ms)\twith message: \"%s\"\r\n", (uintmax_t)newtime_ms, (uintmax_t)(newtime_ms-oldtime_ms), message );
    }
#else
    void Debug_CPUtime( char *message ){}
#endif //DEBUG_PERFORMANCE

void Debug_function( void )
{
    Just_take_a_damn_picture();
}
