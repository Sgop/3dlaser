//==============================================================================
//  Author      : Peter Buijs
//  Project     : 3D_Scanner
//  Description : Header voor Hoofdprogramma
//==============================================================================

//  defines
#define STORAGE_VOLUME "/srv/ftp/3D-Scans/"

//prototypes
int Set_Progress( int );
int Set_Error( char* );
