//==============================================================================
//  Author      : Peter Buijs
//  Description : Structure for containing 3D scan data
//                Similar to OpenCV's IplImage, but with 3D point cloud data
//==============================================================================

#ifndef SCAN_DATA_H
#define SCAN_DATA_H
struct Scan_data{
    // Members moeten nog definitief afgesproken worden
    int  Width; // Width of the Data array
    int  Height;// Height of the data array
    float **Data;// pointer to 2D array
                // Can be laser positions (intermediate result)
                // or distances (final result)
/*  float MaxVal;// Maximum data value. Can be used for normalization.
                // MaxVal is not guaranteed to occur in the Data.
*/};

struct Col_data{
    int Height;
    float *Data;
/*  float MaxVal;// Maximum data value. Can be used for normalization.
                // MaxVal is not guaranteed to occur in the Data.
*/};
#endif  // SCAN_DATA_H
