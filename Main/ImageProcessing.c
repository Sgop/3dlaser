//==============================================================================
//  Author      : Peter Buijs
//  Description : Image processing functions for 3D scanner
//  Requires    : Multiple requirements: see function headers.
//  Notes       : Do not forget to deallocate memory when no longer used
//                The .alphaChannel member of IplImage is abused:
//                Stores the brightest subpixel value in the image
//                This is possible becouse OpenCV ignores this member.
//==============================================================================

#include "Debug.h"
#include "FrameGrabberThread.h"
#include "../Laser/Laser.h"
#include "../MOTORdriver/MOTORdriver.h"
#include "Scan_data.h"
#include "opencv2/core/core_c.h"
#include <pthread.h>

//#define DEBUG_LIVE_IMAGES // couses massive overhead.

#define   LIVE_IMGPATH "/srv/ftp/debug/GetImage_Live.png"
#define    REF_IMGPATH "/srv/ftp/debug/0 Reference.png"
#define  LASER_IMGPATH "/srv/ftp/debug/1 WithLaser.png"
#define   DIFF_IMGPATH "/srv/ftp/debug/2 Difference.png"
#define    ENH_IMGPATH "/srv/ftp/debug/3 Enhanced.png"
#define DETECT_IMGPATH "/srv/ftp/debug/4 Detection.png"

#define RED_OFFSET  2   // layout is BGR, so red subpixel is 2 bytes further

#define CAMERAWIDTH 18  // physical distance betwen laser and camera

// Several global variables from other files are required here
extern volatile void *pFrame;
extern pthread_t CameraThread;
extern IplImage *pReferenceImage;

//==============================================================================
//  Description : Takes a picture using the running camera thread
//  Arguments   : Writable pointer to an IplaImage struct
//  Requires    : Handle to camera thread (global)
//  Return      : Error
//==============================================================================
int GetImage_from_CameraThread( IplImage **ppImage )
{
    pFrame = NULL;

Debug_CPUtime( "  GetImage_from_CameraThread  pre-WaitingForThread" );
    while( pFrame == NULL ) // wait for it
        if( pthread_kill(CameraThread,0) != 0 )
            return ERR_THREADDIED;
Debug_CPUtime( "  GetImage_from_CameraThread post-WaitingForThread" );

    // by now, pFrame points to a valid image
    *ppImage = (IplImage *)pFrame;

    #ifdef DEBUG_LIVE_IMAGES
    DebugSaveImg( LIVE_IMGPATH, *ppImage );
    #endif

    return 0;   // no errors
}

//==============================================================================
//  Description : Takes a picture. Period.
//==============================================================================
void Just_take_a_damn_picture( void )
{
    IplImage *pDebug_image;
    GetImage_from_CameraThread( &pDebug_image );

    Debugprint( "Saving Debug_image.png to debug folder in FTP", 0 );
    const int params = 0;
    cvSaveImage( "/srv/ftp/debug/Debug_image.png", pDebug_image, &params );

    cvReleaseImage( &pDebug_image );
}

//==============================================================================
//  Description : Takes two images as arguments, calculates the difference
//                This difference is overwritten over the first image
//  Arguments   : Pointer to the image to be modified
//                Pointer to the reference image (remains untouched)
//  return      : Error
//  Notes       : alphaChannel is abused for maximum existing subpx value
//==============================================================================
int IplImage_Difference( IplImage *pImage, IplImage *pReferenceImage )
{
    int i, Err = 0;
    char *ImgData, *RefData;
    int RefHeight, RefWidth, RefStep, SubPx_Value;
    RefHeight   = pReferenceImage->height;
    RefWidth    = pReferenceImage->width;
    RefStep     = pReferenceImage->widthStep;
    ImgData     = pImage->imageData;
    RefData     = pReferenceImage->imageData;

    // Sanity check: The image dimentions must agree before continuing
    // This is only required during development ande debugging.
    #ifdef DEBUG
    if( pImage->height    != RefHeight ||
        pImage->width     != RefWidth  ||
        pImage->widthStep != RefStep )
    {
        char DebugString[1024];
        sprintf( DebugString, "\
Dimention mismatch:\r\n\
\tImg\tRef\r\n\
Height\t%d\t%d\r\n\
Width\t%d\t%d\r\n\
Step\t%d\t%d",  pImage->height, RefHeight,
                pImage->width, RefWidth,
                pImage->widthStep, RefStep );
        Debugprint( DebugString, ERR_DIMENTIONS );
        return ERR_DIMENTIONS;
    }
    #endif // DEBUG

    // The actual difference calculation
    // Includes setting brightest subpixel value for contrast enhancement
    pImage->alphaChannel = 0;   // init brigtest value to lowest possible
    for( i=0; i<( RefHeight*RefStep ); i++ )
    {
        ImgData[i] = abs( (int)ImgData[i] - (int)RefData[i] );
        if( ImgData[i] > pImage->alphaChannel )
            pImage->alphaChannel = ImgData[i];
    }

    return Err;
}

//==============================================================================
//  Description : Gets a picture, then calculates the difference
//                with a reference image.
//  Arguments   : writable pointer to an IplImage struct
//  Return      : Error
//  Requires    : Function to get images from the camera
//                -- deprecated : Globally available reference image
//                -- instead    : Function tot oggle laser in between images
//  Notes       : alphaChannel is abused for maximum existing subpx value
//==============================================================================
int Get_DifferenceImage( IplImage **ppDifferenceImage )
{
    int Err;
    IplImage *pReferenceImage;  // do not use the global reference image
    // Instead, take two pictures as quickly as possible,
    // toggling the laser in between.

    // 1). Take picture 2). Toggle laser 3). take picture 4). toggle laser
Debug_CPUtime( "  Get_DifferenceImage  pre-GetImage_from_CameraThread" );
    if( Err = GetImage_from_CameraThread( &pReferenceImage ) )
        return Err;
Debug_CPUtime( "  Get_DifferenceImage post-GetImage_from_CameraThread" );
    SetLaser( ON);

//    while( !pin_high(P9_LASER_FB) );    // verify that the laser gets enabled via external feedback pin
    usleep( LASER_WARMUP_US );    // delay to allow the laser to power up

Debug_CPUtime( "  Get_DifferenceImage  pre-GetImage_from_CameraThread" );
    if( Err = GetImage_from_CameraThread( ppDifferenceImage ) )
        return Err;
Debug_CPUtime( "  Get_DifferenceImage post-GetImage_from_CameraThread" );
    SetLaser( OFF );

    DebugSaveImg( REF_IMGPATH, pReferenceImage );
    DebugSaveImg( LASER_IMGPATH, *ppDifferenceImage );

    motor_move(CLOCKWISE);  // not exacly the nicest place to do this,
    // but it should be done ASAP to give the laser time to stop moving

Debug_CPUtime( "  Get_DifferenceImage  pre-IplImage_Difference" );
    // In-place difference calculation of the first image argument
    Err = IplImage_Difference( *ppDifferenceImage, pReferenceImage );
Debug_CPUtime( "  Get_DifferenceImage post-IplImage_Difference" );
    cvReleaseImage( &pReferenceImage ); // reference image is no longer needed
    Free_debug( "pReferenceImage", pReferenceImage );

    return Err;
}

//==============================================================================
//  Description : Gets a difference image and enances its contrast
//                by normalizing the range of pixel values used:
//                Images with little difference get higer cntrast
//  Arguments   : writable pointer to an IplImage struct
//  Return      : Error
//  Notes       : alphaChannel is abused for maximum existing subpx value
//                This function may introduce excessive noise if no laser
//                or other difference is in the image.
//==============================================================================
int Get_EnhancedImage( IplImage **ppEnhancedImage )
{
    #ifdef ENHANCE_IMAGE
    int Err, Height, Step, i;
    float Multiplier;
    char *ImageData;

Debug_CPUtime( " Get_EnhancedImage  pre-Get_DifferenceImage" );
    if( Err = Get_DifferenceImage( ppEnhancedImage ) )
        return Err;
Debug_CPUtime( " Get_EnhancedImage post-Get_DifferenceImage" );
    DebugSaveImg( DIFF_IMGPATH, *ppEnhancedImage );

    // make life a little easier and code more readable:
    Height      = (*ppEnhancedImage)->height;
    Step        = (*ppEnhancedImage)->widthStep;
    Multiplier  = (float)255 / (*ppEnhancedImage)->alphaChannel;
    ImageData   = (*ppEnhancedImage)->imageData;

    // the actual enhancement, using integer multipliation through floats
Debug_CPUtime( " Get_EnhancedImage  pre-Enhancement" );
    for( i = 0; i < (Height*Step); i++ )
        ImageData[i] = floor( ImageData[i]*Multiplier + 0.5 );
Debug_CPUtime( " Get_EnhancedImage post-Enhancement" );

    return Err;
    #else   // Do not enhance the image, just pass it through
    return  Get_DifferenceImage( ppEnhancedImage );
    #endif  //ENHANCE_IMAGE
}

//==============================================================================
//  Description : Gets an enhanced image and detects the laser position
//                by determining the X-position with the most difference
//                and does this for each row of pixels.
//  Arguments   : writable pointer to a Col_data struct
//  Return      : Error
//==============================================================================
int Get_LaserPosition( struct Col_data **ppLaserPosition )
{
    int Err, X, Y, RowMax=0, MaxVal=0, WidthStep, ChanCount;
    IplImage *pEnhancedImage;
    char *ImgData;

Debug_CPUtime( "Get_LaserPosition  pre-Get_EnhancedImage" );
    if( Err = Get_EnhancedImage( &pEnhancedImage ) )
        return Err;
Debug_CPUtime( "Get_LaserPosition post-Get_EnhancedImage" );
    DebugSaveImg( ENH_IMGPATH, pEnhancedImage );

    ImgData = pEnhancedImage->imageData;
    WidthStep = pEnhancedImage->widthStep;  // row size in bytes (may include padding)
    ChanCount = pEnhancedImage->nChannels;   // numnber of channels (should be 3 for B, G and R)
    // Try to allocate a struct Col_data
    if( !( *ppLaserPosition = malloc( sizeof(struct Col_data) ) ) )
        return ERR_MALLOC;
    Malloc_debug( "*ppLaserPosition", *ppLaserPosition, sizeof(struct Col_data) );
    // Try to allocate an array of floats for the width of the image, assign it's pointer to the Col_data.Data member
    if( !( (*ppLaserPosition)->Data = malloc( pEnhancedImage->height * sizeof(float) ) ) )
        return ERR_MALLOC;
    Malloc_debug( "(*ppLaserPosition)->Data", (*ppLaserPosition)->Data, pEnhancedImage->height*sizeof(float) );

    (*ppLaserPosition)->Height = pEnhancedImage->height;    // set height before starting to set any actual data

Debug_CPUtime( "Get_LaserPosition  pre-Laserdetection" );
    for( Y=0; Y < pEnhancedImage->height; Y++ )
    {
        for( X=0, MaxVal=0; X < pEnhancedImage->width; X++ )
        {
            if( ImgData[Y*WidthStep + X*ChanCount + RED_OFFSET] >= RowMax )   // if multiple, detect last one
            {
                RowMax = ImgData[Y*WidthStep + X*ChanCount + RED_OFFSET];
                (*ppLaserPosition)->Data[Y] = (float)X;
                MaxVal = (RowMax > MaxVal) ? RowMax : MaxVal; // store the largest value found in the entire image
            }
            #if defined(DEBUG_IMAGES) || defined(DEBUG_DETECTION)
            // black out all subpixels, everywhere
            ImgData[Y*WidthStep + X*ChanCount + 0] = 0; // B subpixel
            ImgData[Y*WidthStep + X*ChanCount + 1] = 0; // G subpixel
            ImgData[Y*WidthStep + X*ChanCount + 2] = 0; // R subpixel
            #endif
        }
        RowMax = 0;
        #if defined(DEBUG_IMAGES) || defined(DEBUG_DETECTION)
        // make detected laser pixels white
        ImgData[Y*WidthStep + (int)(*ppLaserPosition)->Data[Y]*ChanCount + 0] = 255;   // B subpixel
        ImgData[Y*WidthStep + (int)(*ppLaserPosition)->Data[Y]*ChanCount + 1] = 255;   // B subpixel
        ImgData[Y*WidthStep + (int)(*ppLaserPosition)->Data[Y]*ChanCount + 2] = 255;   // B subpixel
        #endif
    }
Debug_CPUtime( "Get_LaserPosition post-Laserdetection" );

    DebugSaveImg( DETECT_IMGPATH, pEnhancedImage );
    #ifdef DEBUG_DETECTION
    // Even without debuggin all images, give an option to export this one
    Debugprint( DETECT_IMGPATH, 0 );
    const int params = 0;
    cvSaveImage( DETECT_IMGPATH, pEnhancedImage, &params );
    #endif //DEBUG_DETECTION

    cvReleaseImage( &pEnhancedImage ); // it is no longer needed and thus, deallocated.
    Free_debug( "pEnhancedImage", pEnhancedImage );

    return Err;
}

//==============================================================================
//  Dscription  : Gets the laser-line position
//                and calculates a distance from it (Z coordinate)
//  Arguments   : Writable pointer to Col_data struct
//  Return      : Error
//==============================================================================
//Scan one full comlumn of data points ([Y, Z] coordinates)
int Scan_Column( struct Col_data **ppCurrent_Column )
{
    int Err=0, i;
   // float *Data;
    double hoekLaser, hoekCamera, hoekPunt;
    const double cameraWidth = CAMERAWIDTH*1.0; // if changeing, check validation
    const double PI = 3.14159265358979323846;
    double length;
    float answer;

    if( Err = Get_LaserPosition( ppCurrent_Column ) )
        return Err;
    float *Data = (*ppCurrent_Column)->Data;


    // calculate angle for the laser from external step_count (MOTORdriver.c)
	hoekLaser = 90.0 + ( 45.0 / 2.0 ) - (((double)step_count) / 4096.0*360);

    for ( i = 0; i<IMAGE_HEIGHT; i++) {
        // calcuate angle of dot in camera image. uses width and pos to calulate
        hoekCamera = (90.0 + ( 45.0 / 2.0 ) - (((double)Data[i]) *  45.0 / 640.0 ));

        // Third angle is the result of subtration between 180 and the others 
        hoekPunt = 180.0 - hoekLaser - hoekCamera;
//if (i >= 220 || i<= 240)
//      fprintf(stderr,"C:%+4.0fL:%+4.0fP:%+4.0f D:%+5.2f\t",hoekCamera,hoekLaser,hoekPunt,Data[i]);

        // some math according to law of sines. calculates the length of one side.
        length = (cameraWidth /( sin( hoekPunt *  PI/180.0 ))) * (sin( hoekCamera * PI / 180.0 ));
//      Data[i] = (float)(length);

        // calculates the H from the length.
        Data[i] = (float)((sin( hoekLaser * PI/180.0 ) * length));

        // Checks if data is in range. if not, will make it background
	    if ( Data[i] >= 1100.0 || Data[i] <= -1100.0 )
	        Data[i] = -1100.0; //needs to change when cameraWidth changes

//if (i >= 220 || i<= 240)
//	    fprintf(stderr, "%+5.2f \n", Data[i]);
    }
//    fprintf(stderr, "\n");
    return Err;
}
