//==============================================================================
//  Author      : Peter Buijs
//  Descripton  : Header for LCD related functions, constants, definitions, etc.
//==============================================================================

// external funtions
int LCD_Print( const char *, const char * ); // prints the top and bottom line of the LCD

// types
enum LCD_index{
    LCD_STATUS,
    LCD_STORAGE,
    LCD_ERRORS,

    LCD_SCREENCOUNT // MUST BE LAST!
    // (it is assumed to be the highest index, used in multiple places)
};

// defines
#define LCD_HEIGHT   2  // lines
#define LCD_WIDTH   16  // characters per line
#define LCD_INITSTR_16 "                "   // 16 character string of spaces to init the LCD with whitespace
#define LCD_HEADER_STRINGS {"3D SCAN STATUS  ", "STORAGE FREE    ", "ERRORS          "}
