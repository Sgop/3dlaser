//==============================================================================
//  Author      : Peter Buijs
//  Description : Thread continuously grabs frames from the camera to keep it on
//                A pointer is used for communication and as a mutex:
//                When it is externally set to NULL a frame is stored
//                When ready, the pointer is updated to dereference the frame
//                After this, the frame & memory becomes someone else's problem
//                Do not forget to deallocate it!
//                Reset the pointer to NULL to retrieve a new frame from here.
//  Sources     : docs.opencd.org/java/org/opencv/highgui/VideoCapture.html
//                Basic example of capturing a frame using default opencv
//==============================================================================

#include "opencv2/highgui/highgui_c.h" // includes core_c.h
#include "FrameGrabberThread.h"
#include "Debug.h"
#include <stdbool.h>

//#define LIVESTREAM    // may be anabled to save each and every frame
//Warning: this is VERY SLOW and serves no useful purpose, other than hogging disk space.

#ifdef DEBUG
#ifdef LIVESTREAM
#define LIVESTREAM_PATH "/srv/ftp/debug/LiveStream_%4d.jpg"
int LiveStream_Index = 0;
#endif
#endif

IplImage *CaptureFrame( CvCapture *camera )
{
    IplImage *OpenCV_Frame;
Debug_CPUtime( "   Decode  pre-retrieve" );
    if( !(OpenCV_Frame=cvQueryFrame( camera )) )
        return NULL;    // failed, do not set the pointer
Debug_CPUtime( "   Decode post-retrieve" );
    Malloc_debug( "OpenCV_Frame", OpenCV_Frame, 0 );

Debug_CPUtime( "   Decode  pre-clone" );
    OpenCV_Frame =  cvCloneImage( OpenCV_Frame );    // the original MAY NOT be modified or released
Debug_CPUtime( "   Decode post-clone" );
    return OpenCV_Frame;
}

void *FrameGrabberThread( void *ppFrame )
{
    CvCapture *camera;  // Black box struct for use with C API of C++ functions
#ifdef DEBUG
#ifdef LIVESTREAM
char LiveStream_Path[1024];
#endif
#endif
    if( !(camera=cvCaptureFromCAM(0)) )    // Call constructor with default CAM
        pthread_exit( (void *) ERR_OPENCAM );

    while( true )
    {
        // Continuously grab frames to keep the camera rolling
        if( !cvGrabFrame( camera ) ) // The grab function does not decode the compressed image, it is not needed.
            pthread_exit( (void *) ERR_GRABFRAME ); // may leak memory. But when this happens, it's the least of your worries.

        // if pFrame == NULL, a frame should be captured
        if( *(void **)ppFrame == NULL )
            *(void **)ppFrame = CaptureFrame( camera );
        #ifdef LIVESTREAM
        DebugSaveImg( LiveStream_Path, *(IplImage **)ppFrame );
        #endif

        // Check if we are done filming yet
        if( *(void **)ppFrame == FGTHREAD_DIE )
            break;
    }

    cvReleaseCapture( &camera );// Call destructor
    Debugprint( "Thread exiting with no errors", 0 );
    pthread_exit( NULL );       // Return no errors
}
