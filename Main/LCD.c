//==============================================================================
// Author       : Peter Buijs
// Description  : Dummy function for LCD_Print() to test Hoofdprogramma
//==============================================================================

#include "LCD.h"
#include "Debug.h"
#include "../filegpio/LCDdriver.h"

#include <stdio.h>
#include <string.h>

#define LCD_DEBUGFORMAT "\
 ------------------ \r\n\
| %s |\r\n\
| %s |\r\n\
 ------------------ \r\n"

int LCD_Print( const char *Top_Line, const char *Bottom_Line )
{
    char Top_Line_16[17], Bottom_Line_16[17];

    if( (strlen(Top_Line) > LCD_WIDTH) || (strlen(Bottom_Line) > LCD_WIDTH) )
        return ERR_LCD_WIDTH;
    else
    {
        sprintf(    Top_Line_16, "%*.*s", LCD_WIDTH, LCD_WIDTH,    Top_Line ); // print exactly LCD_WIDTH characters
        sprintf( Bottom_Line_16, "%*.*s", LCD_WIDTH, LCD_WIDTH, Bottom_Line ); // print exactly LCD_WIDTH characters

        // this section needs replacement. (Also, edit the header when you do!)
        printf( LCD_DEBUGFORMAT, Top_Line_16, Bottom_Line_16);
        LCD_writelines( Top_Line_16, Bottom_Line_16 );
    }

    return 0;
}
