//==============================================================================
//  Author      : Peter Buijs
//  Description : Exposes FrameGrabberThread() for external use
//==============================================================================

#define FGTHREAD_INIT   (void *) 1  // Pointer value to initialize with
#define FGTHREAD_DIE    (void *)-1  // Pointer value to signal a kill
                                    // I know, this is a complete hack.
                                    // Please don't complain...

// FRAME_ or IMAGE_ may be use at-will, they are the same.
//#define FRAME_WIDTH     640
#define FRAME_WIDTH     240 // fixme: determine horizontal resolution (steps/degrees/millimeters)
#define FRAME_HEIGHT    480
#define FRAME_CHANNELS  3
#define IMAGE_WIDTH     FRAME_WIDTH
#define IMAGE_HEIGHT    FRAME_HEIGHT
#define IMAGE_CHANNELS  FRAME_CHANNELS

#ifdef __cplusplus
extern "C" {
#endif
// FrameGrabberThread: keeps the camera running and takes pictures at-will.
// Argument: Pointer to a modifyable frame pointer.
// use: see description in code header
void *FrameGrabberThread( void * ); // C compatible C++ function
#ifdef __cplusplus
}
#endif
