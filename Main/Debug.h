//==============================================================================
//  Author      : Peter Buijs
//  Description : header for debugging using Debugprint()
//==============================================================================

#include <stdio.h>

#ifndef DEBUG_H
#define DEBUG_H

//#define ENHANCE_IMAGE // enable or disable image enhancements

// Comment the lines below to disable debugging
#define DEBUG
//#define DEBUG_IMAGES
//#define DEBUG_DETECTION
//#define DEBUG_MEMLEAK
//#define DEBUG_MANUALLASER
#define DEBUG_PERFORMANCE

// all errors are 1 bit flags
// All errors can be OR'd together
#define ERR_NYI         (1 << 0)    //   1 Not Yet Implemented
#define ERR_CASEDEF     (1 << 1)    //   2 Default case was run, not all cases covered
#define ERR_LCD_WIDTH   (1 << 2)    //   4 Printed strings do not match LCD width
#define ERR_BUFSIZE     (1 << 3)    //   8 Buffer not large enough
#define ERR_STATVFS     (1 << 4)    //  16 Error getting filesystem statistics from OS
#define ERR_THREADDIED  (1 << 5)    //  32 A thread died prematurely
#define ERR_OPENCAM     (1 << 6)    //  64 Could not open camera
#define ERR_GRABFRAME   (1 << 7)    // 128 Could not grab a frame
#define ERR_DIMENTIONS  (1 << 8)    // 256 Image dimentions are not equal
#define ERR_MALLOC      (1 << 9)    // 512 Malloc was unsuccesful
#define ERR_QUITPLEASE  (1 <<10)    //1024 Error to signal a graceful exit
#define ERR_SCANNING    (1 <<11)    //2048 Error during scanning, see other errors
#define ERR_SAVING      (1 <<12)    //4096 Error during saving, see other errors
#define ERR_CALIBRATING (1 <<13)	//8192 Error during calibrating
/* copy paste template:
#define ERR_    (1 <<)    //
*/

// prototypes
char *Get_ErrorText( int Err );
void Malloc_debug( char *Message, void *Ptr, int Size );
void Free_debug( char *Message, void *Ptr );
void Debug_CPUtime( char *message );
void Debug_function( void );    // sandbox function for debugging.
// May be usede for any debugging code and may be replaced without warning

//==============================================================================
//  Debugprint macro Prints a debug message and err number if DEBUG is defined
//==============================================================================
#ifdef DEBUG
    #define Debugprint( Message, Err ) \
    fprintf( stderr, "%s %s (code %d: %s)\r\n",Err?"[ERROR]":"[INFO ]", Message, Err, Get_ErrorText(Err) )
#else
    #define Debugprint( Message, Err ) // empty macro
#endif // DEBUG

#endif // DEBUG_H
