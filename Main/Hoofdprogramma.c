//==============================================================================
//  Author      : Peter Buijs
//  Project     : 3D_Scanner
//  Description : Hoofdprogramma en User I/O voor de 3D laser scanner
//==============================================================================

//  Includes
#include <stdlib.h> // exit()
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/statvfs.h>

#include "Hoofdprogramma.h"
#include "Scan_data.h"
#include "ScannerFunctions.h"
#include "LCD.h"
#include "FrameGrabberThread.h"
#include "../filegpio/filegpio.h"
#include "../Laser/Laser.h"
#include "../plyfile/plylib.h"
#include "../MOTORdriver/MOTORdriver.h"
#include "Debug.h"

//  Prototypes
int Button_Start_Scan( void );
int Button_Scroll( void );
int LCD_Refresh( void );
int Get_Status( char[], int );
int Get_Storage( char[], char*, int );
int Motor_Start_Cal( void );

bool IsPressed_Start_Scan( int* );
bool IsPressed_Scroll( int* );

// globals
bool Scan_in_progress = false;
int  Progress_percentage = 0;
enum LCD_index Current_screen = LCD_STATUS;
char *Current_error = "No Errors";  // Use for user-understandable errors only
                                   // i.e. "Storage full"
// Worker thread globals:         // (Not for software bugs / debugging)
// Pointer for communication with frame grabber thread
// This pointer is not always valid, even when not NULL!
volatile void *pFrame;
pthread_t CameraThread; // Camera thread handle

//==============================================================================
//  Main() : Hoofd programmalus voor de 3D laser scanner
//==============================================================================
int main( void )
{
    // initialization
    int Err=0, i;
    // Get the camera rolling by forking a worker thread
    void *Thread_returnval;
    pFrame = FGTHREAD_INIT; // Make sure it is not instantly killed
    if( pthread_create(&CameraThread,NULL,FrameGrabberThread,&pFrame) )
        Debugprint( "Thread failed to start", (Err |= ERR_THREADDIED) );
    else
        Debugprint( "Thread started", 0 );
    // LCD & Laser
    LCD_init();
    Err |= LCD_Refresh();
    Init_Laser();
//    SetLaser( OFF );  // Fixme: very annoying at startup when the laser is set to manual.
    // gpio for buttons (scroll & start scan respectively)
    export_gpio( NEXTPIN );
    export_gpio( STARTPIN );
    set_direction( NEXTPIN, DIR_IN );
    set_direction( NEXTPIN, DIR_IN );
    // feedback for user
    if( Err )
    {
        Debugprint( "Error in main initialization", Err );
        return Err;
    }
    else
        printf("Scanner started.\r\nUse the buttons to start a scan (black) or scroll the display (white).\r\n");
    // end of initialization

    // start of main loop
    while( true )
    {
        if( pin_high( STARTPIN ) )
        {
            if( Err = Button_Start_Scan() )
                break;
            printf("Use the buttons to start a scan (black) or scroll the display (white).\r\n");
        }

        if( pin_high( NEXTPIN ) )
        {
            while( pin_high( NEXTPIN ) )    // wait until it is released
                #ifdef DEBUG
                if( pin_high( STARTPIN ) )  // in the meantime press start
                    Debug_function()        // to call a debug function
                #endif
                ; // DO NOT forget this semicolon after the above function
            if( Err = Button_Scroll() )
                break;
            printf("Use the buttons to start a scan (black) or scroll the display (white).\r\n");
        }

        usleep( 10*1000 ); // give Linux some rest
    }
    // handle errors
    fprintf( stderr, "%s%d\r\n", "Whoops! The following error is not handled in main(): ", Err );

    // Gracefully kill the camera thread
    pFrame = FGTHREAD_DIE;
    pthread_join( CameraThread, &Thread_returnval ); // wait for it to die
    Err |= (int)Thread_returnval;   // Does not actually return void* but int

    fprintf( stderr, "Bye now!\r\n" );
    return Err; // catch thread's return value. Should be 0 (succes)
}

//==============================================================================
//  Button_Start_Scan() : Afhandeling van de ingedrukte scan knop
//==============================================================================
int Button_Start_Scan( void )
{
    int Err, i;
    struct Scan_data *pCurrent_scan;

	if( Err = Motor_Start_Cal() ) 
		return (Err | ERR_CALIBRATING);


    Scan_in_progress = true;
    Progress_percentage = 0;
    Current_screen = LCD_STATUS;    // scroll LCD to display status
    LCD_Refresh();

    if( Err = Scan_3D( &pCurrent_scan ) )// perform an actual scan
        return (Err | ERR_SCANNING);
    Progress_percentage = 100;
    LCD_Refresh();
    if( Err = Save_PLY( *pCurrent_scan ) )// save to file
        return (Err | ERR_SAVING);

    // cleanup
    for( i=0; i<IMAGE_WIDTH; i++ )
    {
        free( pCurrent_scan->Data[i] );
        Free_debug( "pCurrent_scan->Data[i]", pCurrent_scan->Data[i] );
    }
    free(pCurrent_scan->Data);
    Free_debug( "pCurrent_scan->Data", pCurrent_scan->Data );
    free(pCurrent_scan);
    Free_debug( "pCurrent_Scan", pCurrent_scan );
    Scan_in_progress = false;
    LCD_Refresh();

    return Err; // Catch errors in the free() functions (not expected to happen)
}

//==============================================================================
//  Button_Scroll() : Afhandeling van de ingedrukte scroll knop
//==============================================================================
int Button_Scroll( void )
{
    int Err=0;

    // increment screen index & check it's upper bound
    if( ++Current_screen >= LCD_SCREENCOUNT )
        Current_screen = 0;

    if( Err |= LCD_Refresh() )
        Debugprint( "Error while refreshing LCD", Err );

    return Err;
}

//==============================================================================
//  LCD_Refresh() : ververst de tekst op het LCD aan de hand van actieve venster
//==============================================================================
int LCD_Refresh( void )
{
    int Err=0;
    const char pHeader_str_16[LCD_SCREENCOUNT][LCD_WIDTH+1] = LCD_HEADER_STRINGS;
    char Data_str_16[LCD_WIDTH+1] = ""; // +1 to allow for \0 termination

    switch( Current_screen )
    {
        case LCD_STATUS :
            Err |= Get_Status(                  Data_str_16, LCD_WIDTH );
            break;
        case LCD_STORAGE:
            Err |= Get_Storage( STORAGE_VOLUME, Data_str_16, LCD_WIDTH );
            break;
        case LCD_ERRORS :
            // Copy Current_error to Data_str_16, leave 16-char space padding
            strncpy( Data_str_16, Current_error, strlen(Current_error)>LCD_WIDTH ? LCD_WIDTH : strlen(Current_error) );
            break;
        default         :
            return ERR_CASEDEF;
    }

    if( Err!=0 && Err!=ERR_NYI ) return Err;   // do not continue on error

    Err |= LCD_Print( pHeader_str_16[Current_screen], Data_str_16 );

    return Err;
}

//==============================================================================
//  Get_Status() : Converteert huidige scanner status/progress naar een string
//==============================================================================
int Get_Status( char Status_Str[], int Status_Str_Length )
{
    int Err=0;

    // genereer exact Status_Str_Length karakters voor LCD (ex. \0)
    if( Scan_in_progress == true )
        sprintf( Status_Str, "%*d%*.*s",   // concaternate number & units
            Status_Str_Length/2                     , Progress_percentage,
            Status_Str_Length/2, Status_Str_Length/2, " PERCENT" );
    else    // no scan in progress, Ready to start a scan
        sprintf( Status_Str, "%*.*s",
            Status_Str_Length, Status_Str_Length, "READY" );

    return Err;
}

//==============================================================================
//  Get_Storage() : Vraagt Linux om beschikbare ruimte & converteert naar string
//==============================================================================
int Get_Storage( char *Volume_Str, char FreeSpace_Str[], int FreeSpace_Str_Length )
{
    int Err=0;
    float FreeSpace_GB = 0;
    struct statvfs Filesystem_Statistics;

    // check available space with the OS
    if( statvfs( Volume_Str, &Filesystem_Statistics ) )
        Err = ERR_STATVFS;
    else
        FreeSpace_GB = ( (float)Filesystem_Statistics.f_bsize * (float)Filesystem_Statistics.f_bfree ) / ( 1024 * 1024 * 1024 );

    // genereer exact FreeSpace_Str_Length karakters voor LCD (ex. \0)
    sprintf( FreeSpace_Str, "%*.3f%*.*s",   // concaternate number & units
        FreeSpace_Str_Length/2-1                          , FreeSpace_GB,
        FreeSpace_Str_Length/2+1, FreeSpace_Str_Length/2+1, " GIGABYTE" );
        // Note: the float may overflow when >= 1000GB
        // printf cannot limit the integer part of a printed float

    return Err;
}

//==============================================================================
//  Set_Progress() : Verandert het progressiepercentage tijdens een lopende scan
//==============================================================================
int Set_Progress( int Progress_Percent )
{
    int Err = 0;
    if( Progress_percentage != Progress_Percent )
    {
        Progress_percentage = Progress_Percent;
        Err = LCD_Refresh();
    }
    return Err;
}

//==============================================================================
//  Set_Error() : Verandert het foutbericht voor op het LCD en geeft direct weer
//==============================================================================
int Set_Error( char* ErrorMessage )
{
    int Err = 0;

    // code

    return Err;
}

//==============================================================================
//  Motor_Start_Cal() : Calibreert Motor startpositie
//==============================================================================
int Motor_Start_Cal( void )
{
    float stepsToStart;
	int Err = 0;
	int state = 1;
	int counter = 1000;
	int i;
	char Data_str_16_1[17] = "Rotate CCW?     ";
	char Data_str_16_2[17] = " black= continue";
    usleep(1000);
    SetLaser(1);

    stepsToStart = 22.5 / (360.0/4096.0)  ;

    for (i = 0 ; i < stepsToStart; i++)
        motor_step(CLOCKWISE);

    
	if (Err = LCD_Print(Data_str_16_1, Data_str_16_2))
		return Err;

		while ( pin_low(STARTPIN) ) {
			if ( pin_high(NEXTPIN) ) {
				motor_step(CCLOCKWISE);
				}
            counter++;
		    if (counter >= 1000)
		    {
		        switch (state)
		        {
		            case 1:
		                strcpy(Data_str_16_1, "Set laser to str");
                	    if (Err = LCD_Print(Data_str_16_1, Data_str_16_2))
	                	    return Err;

		                state = 2;
		                break;
		            case 2:
		                strcpy(Data_str_16_1, "traight w/ white");
	                    if (Err = LCD_Print(Data_str_16_1, Data_str_16_2))
	                	    return Err;

		                state = 1;
		                break;
		            default:
		                state = 1;
		        }
                counter = 0;
			} 

		usleep(1000);

		}
	SetLaser(0);

    for (i = 0 ; i < stepsToStart; i++)
        motor_step(CCLOCKWISE);

	return Err;
}
