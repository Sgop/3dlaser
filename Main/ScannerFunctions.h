//=============================================================================
//  Author      : Peter Buijs
//  Description : Public functions for use in the 3D_Scanner project
//=============================================================================

#include "Scan_data.h"

int Scan_3D( struct Scan_data** );
int Save_PLY( struct Scan_data );
