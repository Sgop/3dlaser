//==============================================================================
//  Author      : Peter Buijs
//  Decription  : GPIO library wrapper for use of the laser toggle pin
//==============================================================================

#include "Laser.h"
#include "../Main/Debug.h"

void Init_Laser( void )
{
    export_gpio( P9_LASER );
    set_direction( P9_LASER, DIR_OUT );

    export_gpio( P9_LASER_FB );
    set_direction( P9_LASER_FB, DIR_IN );
}

void SetLaser( int State )
{
#ifdef DEBUG_MANUALLASER
    if( State == ON )
        Debugprint( "Turning the laser ON", 0 );
    else
        Debugprint( "Turning the laser OFF", 0 );

    printf( "(Note: you may want to actually toggle the laser now,\r\nespecially since it is not even connected yet.\r\nPress enter when done...)" );
    getchar();
#endif //DEBUG_MANUALLASER

    write_out( P9_LASER, State );
}
