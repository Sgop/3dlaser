//==============================================================================
//  Author      : Peter Buijs
//  Description : Header for simplified Laser control via GPIO
//==============================================================================

#include "../filegpio/filegpio.h"

#define ON  1
#define OFF 0
#define LASER_WARMUP_US (20*1000)   // laser warmup time in microseconds
// Turn the laser ON or OFF.
// Returns 0 on succes, error otherwise
void SetLaser( int );

