
/* motordriver */
#define CLOCKWISE   0
#define CCLOCKWISE  1

#define MOVESTEPS   2   // fixme: determine horizontal resolution in degrees/steps (see same fixme comment in Imageprocessing.h)

#define MAXSTEP     8

#define STEPVALUE0	0x1
#define STEPVALUE1	0x3
#define STEPVALUE2	0x2
#define STEPVALUE3	0x6
#define STEPVALUE4	0x4
#define STEPVALUE5	0xC
#define STEPVALUE6	0x8
#define STEPVALUE7	0x9

#define STARTPIN	65
#define NEXTPIN		48
	
#define BEGINGRADEN ( 90 - ( 45 / 2 ) )


/*
for 4step:
change STEPVALUE0 till 3 to: 0x1 2 4 8
and change maxstep to 4
*/

// prototypes
extern int step_count;

void motor_step( int );

void motor_value( int );

void motor_init( void );

void motor_return( void );
