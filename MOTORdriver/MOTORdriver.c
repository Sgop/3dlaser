
/* motordriver */
#include "MOTORdriver.h"
#include "../filegpio/filegpio.h"
#include <unistd.h>
#include <stdio.h>

//globals
int motorgpio[] = {30,60,31,50};
int step = 0;

int step_count = 0;

//functions
void motor_step(int direction){
	if ( direction == CLOCKWISE ) { // check for next and check for overflow
		if (step >= ( MAXSTEP - 1) ) {
			step = 0;
		} else {
			step++;
		}
//        fprintf(stderr, "rotate Clockwise %d %X\n",direction, step);
		step_count++;
	} else if ( direction == CCLOCKWISE) { // CCW
		if (step <= 0) {
			step = ( MAXSTEP - 1);
		} else {
			step--;
		}
//        fprintf(stderr, "rotate Counter-Clockwise %d %X\n",direction, step);
		step_count = 0;
	} else {
	    fprintf(stderr, "error while stepping %d\n", direction);
	}
	switch ( step ) { // switch statement for next stepvalue
		case 0 : 
			motor_value(STEPVALUE0);
		break;
		case 1 :
			motor_value(STEPVALUE1);
		break;
		case 2 :
			motor_value(STEPVALUE2);
		break;
		case 3 :
			motor_value(STEPVALUE3);
		break;
		case 4 :
			motor_value(STEPVALUE4);
		break;
		case 5 :
			motor_value(STEPVALUE5);
		break;
		case 6 :
			motor_value(STEPVALUE6);
		break;
		case 7 :
			motor_value(STEPVALUE7);
		break;
	}
	usleep( 5000 );
}

void motor_value(int dataout) {
	int buffer = 0;
	int i,j,k;
#ifdef DEBUG
		printf ("\n0x%03X\n", dataout);
#endif
	// zet data op IO, pulse uit TODO
	for ( i =0 ; i < 4; i++){
		buffer = dataout;
		buffer &= 1 << ( 3 - i );
		if ( buffer >= 1 ) {			// check if bitmask 
			
			write_out(motorgpio[i], 1);
			k = 1;//pin[1] hoog 	
		}
		else {
			write_out(motorgpio[i], 0);
			k = 0;//pin[i] laag 
		}
		
#ifdef DEBUG
		j = /* 9 - */ i;
		printf( "%d:%s=%X " , j, gpionaam[j], k );
#endif
			
	}
#ifdef DEBUG
	printf("\n");
#endif
}
		
 
void motor_init() { 
	int i;
	for (i = 0; i < 4; i++) {
		export_gpio(motorgpio[i]);
		set_direction(motorgpio[i], 1);
	}
	motor_value(STEPVALUE0);

}

void motor_move(int direction)
{
    int i;
    for( i=0; i<MOVESTEPS; i++ )
        motor_step(direction);
    return;
}

void motor_return()
{
    int i;
	int step_count_buffer = step_count;
	fprintf( stderr, "motor_return() is returning %d steps.\r\n", step_count );
	for ( i = 0; i < step_count_buffer; i ++)
		motor_step(CCLOCKWISE);
    return;
}
