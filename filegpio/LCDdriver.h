/*
LCD driver */

/*
Instruction	Code										Execution
														time (max)
					RS	R/W	B7	B6	B5	B4	B3	B2	B1	B0	(when fcp = 270 kHz)
Clear display		0	0	0	0	0	0	0	0	0	1	1.52 ms
Cursor home			0	0	0	0	0	0	0	0	1	*	1.52 ms
Entry mode set		0	0	0	0	0	0	0	1	I/D	S	37 μs
Display on/off 		0	0	0	0	0	0	1	D	C	B	37 μs
Crsr/dspl shift		0	0	0	0	0	1	S/C	R/L	*	*	37 μs
Function set		0	0	0	0	1	DL	N	F	*	*	37 μs
Set CGRAM address	0	0	0	1	ad	d 	r	e	s	s	37 μs
Set DDRAM address	0	0	1	a	d	d	r	e	s	s	37 μs
Write CGRAM or		1	0	Write Data						37 μs
DDRAM
Read from CG/DDRAM	1	1	Read Data						37 μs

I/D - 0 =	decrement cursor position, 1 = increment cursor position
 S - 0 =	no display shift, 1 = display shift
 D - 0 =	display off, 1 = display on
 C - 0 =	cursor off, 1 = cursor on
 B - 0 =	cursor blink off, 1 = cursor blink on
 S/C - 0 =	move cursor, 1 = shift display
 R/L - 0 =	shift left, 1 = shift right
 DL - 0 =	4-bit interface, 1 = 8-bit interface
 N - 0 =	1/8 or 1/11 duty (1 line), 1 = 1/16 duty (2 lines)
 F - 0 =	5×8 dots, 1 = 5×10 dots

 instructies tabel van http://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller
*/


// debug prints io in the console.

//prototypes


void LCD_init();
void LCD_CONFIG();
void LCD_write_out( unsigned int dataout );
void LCD_writelines(unsigned char line1[16], unsigned char line2[16] ) ;
void LCD_CLEAR() ;
void LCD_clearline(unsigned int linenr) ;
void pulse(int port);
