// new gpio lib
// made by example to http://falsinsoft.blogspot.nl/2012/11/access-gpio-from-linux-user-space.html
// JZ

// GPIO direction setting
#define DIR_OUT      1
#define DIR_IN       0

// GPIO pin mapping
#define NEXTPIN		48
#define STARTPIN	65

#define P8_07	    66
#define P8_08	    67
#define P8_09	    69
#define P8_10	    68
#define P8_11	    45
#define P8_12	    44
#define P8_13	    23
#define P8_14	    26
#define P8_15	    47
#define P8_16	    46
#define P8_17	    27
#define P8_18	    65
#define P8_19	    22

#define P9_LASER    15   //P9_21
#define P9_LASER_FB 3   //P9_22

#define P9_23		49
#define P9_24		15
#define P9_26		14
#define P9_27		115

// GPIO functions

void export_gpio(int gpio) ; 
// wants gpio number as written down in ...

void set_direction(int gpio, int dir);
// sets direction for gpio after export_gpio. dir = 1 for output, 0 for input. (no pullup/down yet)

void write_out(int gpio, int out) ;
// sets gpio value to logic high or low acc to out. 1 for high 0 for low. 

void read_in (int gpio, int *value) ;

int pin_high (int gpio);

int pin_low (int gpio);

