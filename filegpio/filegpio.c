
/* *****************************************************************************
**	Naam:	FileGPIO														  **
**	  Werkt door op kernelniveau met GPIO te communiceren. Eerst moet de GPIO **
**	  beschikbaar gemaakt worden (export_gpio). Hierna wordt voor de GPIO een **
**	  bestandssysteem aangemaakt (value, direction, etc.) Deze kan vervolgens **
**	  worden uitgelezen. 													  **
**	Datum:	11-03-2014 														  **
**	Door: Jens Zonneveld													  **
**	Naar voorbeeld van: 													  **
**http://falsinsoft.blogspot.nl/2012/11/access-gpio-from-linux-user-space.html *
**  Headerfile:	"filegpio.h"	                                              **
*******************************************************************************/


#include <stdio.h>
#include "filegpio.h"

#define MAX_BUF 40
#define WRITEONLY "w"
#define READONLY "r"

#define DEBUGGPIO 1


/* *****************************************************************************
**	Function: export_gpio													  **
**	Variables: 	int gpio: gpio number										  **
**	Works by: writing gpio number to export file. After that kernel takes 	  **
** 		care of everything.													  **
*******************************************************************************/
void export_gpio(int gpio) {

	FILE * fd;
	char buf[MAX_BUF]; 

	fd = fopen("/sys/class/gpio/export", WRITEONLY); //exportfile location
	if (fd == NULL ){ 						// if file not opened print error
		printf("error opening file \n");
		return;								//close export when error
	}

	fprintf(fd, "%d", gpio); // write gpio nummer to file

	fclose(fd); // close file
} 


/* *****************************************************************************
**	Function: set_direction													  **
**	Variables: 	int gpio: gpio number										  **
**				int dir: Direction of GPIO. 1 for out, 0 for in				  **
**	Works by: writing gpio number to export file. After that kernel takes 	  **
** 		care of everything.													  **
*******************************************************************************/
void set_direction(int gpio, int dir){
	FILE * fd;
	char buf[MAX_BUF]; 

	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);

	fd = fopen(buf, WRITEONLY);

	if ( dir >= 1 )	{
		char str_out[] = "out";
		fputs ( str_out, fd);
	}
	else {
		char str_in[] = "in";
		fputs ( str_in, fd);
	}
	
	fclose(fd);
}

/* *****************************************************************************
**	Function: write_out														  **
**	Variables: 	int gpio: gpio number										  **
**				int out: value for output. <= 0 for low, >= 1 for high 		  **
**	Works by: writing value number to value file in specific gpio folder	  **
**	(located in /sys/class/gpio/) After that kernel takes care of everything. **
*******************************************************************************/
void write_out(int gpio, int out) {
	FILE * fd;
	char buf[MAX_BUF]; 


	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);

	fd = fopen(buf, WRITEONLY);

	if ( out >= 1 ) {
		fprintf(fd, "1");
	}
	else {
		fprintf(fd, "0");
	}
	fclose(fd);
}

/* *****************************************************************************
**	Function: read_in														  **
**	Variables: 	int gpio: gpio number										  **
**				int *value: pointer to value variable 						  **
**	Works by: Reading value file in specific gpio folder. Writes that to the  **
**	location that the pointer points to.									  **
*******************************************************************************/
void read_in (int gpio, int *value) {
	FILE * fd;
	char buf[MAX_BUF];

	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);

    // watches for availability of file. if not, redo till available.
    while (!( fd = fopen(buf, READONLY))){

        nanosleep(100000); // 100 ms delay
	}

	fscanf(fd, "%d", value); // scan for value in file. 

	fclose(fd);
}

/* *****************************************************************************
**	Function: 	pin_high													  **
**	Variables: 	int gpio: gpio number										  **
**	Returns: 	integer. 1 when true, 0 when false		 					  **
**	Works by: Using the read_in function. this functions acts more as a shell **
**	for easyer use in statements like if and while.							  **
*******************************************************************************/
int pin_high(int gpio) {
	int value;
	read_in(gpio, &value);
	if (value >= 1)
		return 1;
	return 0;
}

/* *****************************************************************************
**	Function: 	pin_low														  **
**	Variables: 	int gpio: gpio number										  **
**	Returns: 	integer. 1 when true, 0 when false		 					  **
**	Works by: Using the read_in function. this functions acts more as a shell **
**	for easyer use in statements like if and while.							  **
*******************************************************************************/
int pin_low(int gpio) {
	int value;
	read_in(gpio, &value);
	if ( value == 0 )
		return 1;
	return 0;
}

