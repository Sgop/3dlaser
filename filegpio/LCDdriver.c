/* 
LCD driver */

/* 
Instruction	Code										Execution
														time (max)
					RS	R/W	B7	B6	B5	B4	B3	B2	B1	B0	(when fcp = 270 kHz)
Clear display		0	0	0	0	0	0	0	0	0	1	1.52 ms
Cursor home			0	0	0	0	0	0	0	0	1	*	1.52 ms
Entry mode set		0	0	0	0	0	0	0	1	I/D	S	37 μs
Display on/off 		0	0	0	0	0	0	1	D	C	B	37 μs
Crsr/dspl shift		0	0	0	0	0	1	S/C	R/L	*	*	37 μs
Function set		0	0	0	0	1	DL	N	F	*	*	37 μs
Set CGRAM address	0	0	0	1	ad	d 	r	e	s	s	37 μs
Set DDRAM address	0	0	1	a	d	d	r	e	s	s	37 μs
Write CGRAM or		1	0	Write Data						37 μs
DDRAM											
Read from CG/DDRAM	1	1	Read Data						37 μs

I/D - 0 = decrement cursor position, 1 = increment cursor position
 S - 0 = no display shift, 1 = display shift
 D - 0 = display off, 1 = display on
 C - 0 = cursor off, 1 = cursor on
 B - 0 = cursor blink off, 1 = cursor blink on 
 S/C - 0 = move cursor, 1 = shift display
 R/L - 0 = shift left, 1 = shift right
 DL - 0 = 4-bit interface, 1 = 8-bit interface
 N - 0 = 1/8 or 1/11 duty (1 line), 1 = 1/16 duty (2 lines)
 F - 0 = 5×8 dots, 1 = 5×10 dots
*/
	
#include "filegpio.h"
#include <stdio.h>
//#define DEBUG 0
// debug prints io in the console.

//prototypes

void LCD_init();
void LCD_CONFIG();
void LCD_CLEAR();
void LCD_writelines();
void LCD_clearline();
void LCD_write_out();
void pulse();

int gpio[]= {P8_15,	P8_17,	P8_07,	P8_08,	P8_09,	
//			RS		RW		D7		D6		D5			
			P8_10,	P8_11,	P8_12,	P8_13,	P8_14};
//			D4		D3		D2		D1		D0
#ifdef DEBUG
char gpionaam[][3] = {"RS", "RW", "D7", "D6", "D5", "D4", "D3", "D2", "D1", "D0"};
#endif

int pin_en = P8_16;
unsigned int dataout;


void LCD_init(){
	//LCD_init start GPIO en roept config aan.
	int i;
	
	//init GPIO
	for (i =0; i < 10 ; i++){
		export_gpio(gpio[i]);
		set_direction(gpio[i], 1);		
	}
	export_gpio(pin_en);
	set_direction(pin_en, 1);
	
	// zet configuratie
	LCD_CONFIG();
	
}

void LCD_CONFIG(){



	
	dataout = 0x030;
	LCD_write_out(dataout);
	//wait 4.1ms TODO
	LCD_write_out(dataout);
	// wait 100 us TODO
	LCD_write_out(dataout);
	
	LCD_write_out(0x038); // Function mode
	LCD_write_out(0x008); // Display off
	LCD_write_out(0x001); // display clear
	LCD_write_out(0x006); // Entry Mode set
	LCD_write_out(0x00c); // display on	


	
}
/* functie LCD_write_out werkt als volgt: loop langs de bits met bitmask om 
waarde bit te achterhalen. indien hoog, zet gpio hoog. indien laag zet gpio 
laag dmv value file
*/
void LCD_write_out( unsigned int dataout ){
	int buffer = 0;
	int i,j,k;
#ifdef DEBUG
		printf ("\n0x%03X\n", dataout);
#endif
	// zet data op IO, pulse uit TODO
	for ( i =0 ; i < 10; i++){
		buffer = dataout;
		buffer &= 1 << ( 9 - i );
		if ( buffer >= 1 ) {			// check if bitmask 
			
			write_out(gpio[i], 1);
			k = 1;//pin[1] hoog 	
		}
		else {
			write_out(gpio[i], 0);
			k = 0;//pin[i] laag 
		}
		
#ifdef DEBUG
		j = /* 9 - */ i;
		printf( "%d:%s=%X " , j, gpionaam[j], k );
#endif
			
	}
	pulse(pin_en);
#ifdef DEBUG
printf("\n");
#endif
}

void LCD_writelines(unsigned char line1[16], unsigned char line2[16] ) {
	int i;
	int j = 0;
	int buffer;
	// move cursor to pos 1/0
	//LCD_charmap(*line1);
	//LCD_charmap(*line2);
	
	LCD_write_out(0x080); // move cursor to pos 1 
	for (i=0; i < 16; i++) { 
		if (line1[i] == '\0') {
			j = 1; // if end of string 
		}
		if ( j == 0 ) { //no end of string?
			 // Or operator for writing characters instead of registers
			 buffer = 0x200 | line1[i];
		} else {
			buffer = 0x200; // if end of string then space
		}
		LCD_write_out(buffer);
	}
	
	// move cursor to first of second line (buffer / 2)
	LCD_write_out(0x0C0); 
	//same as above
	j = 0; // reset end of string.
	for (i=0; i < 16; i++) { 
		if (line2[i] == '\0') {
			j = 1;
		}
		if ( j == 0 ) {
			buffer = 0x200 | line2[i]; 
		} else {
			buffer = 0x200;
		}
		LCD_write_out(buffer);
	}
	
};

void LCD_CLEAR() {
	LCD_write_out(0x0001); // display clear	
};

void LCD_clearline(unsigned int linenr) {
	int i;
	if ( linenr > 1 ) {
		LCD_write_out(0x0080);  //locatie naar pos 1
	}
	else {
		LCD_write_out(0x00C0); //locatie 1 van regel 2 (lok 40)
	}
	for (i=0; i < 16; i++){ // 16 maal leeg karakter sturen
		LCD_write_out(0x0200);
	}
}

void pulse(int port){
	usleep(100);
	write_out(port, 1);
	usleep(50);
	write_out(port, 0);
}
	
